#!/bin/bash

Checkfiles(){

Title1=$(head -1 $1)
Title=${Title1##*#}
echo $Title
FileName=$(basename ${1} .md)
echo $FileName

data=0
for i in `cat $1`
do
result=$i
echo $result
if [[ $result =~ "due" ]]
then
   data=$[$data+1]
fi
done

#This is the script to write the todos without date to a new file but it is unnecessary now
:<<!
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
while read line
do
echo $line
if [[ $line =~ "due" ]]
then
   echo $line Have date
else	
	if [[ $line =~ "#" ]]
	then
	echo $line is title
	else
	echo 
	no_due=$line
	echo $no_due
	echo $no_due > test.md
	echo $line moved to no_duedates
	fi
fi
done  < $1
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

echo line of due :  $data 

if [ $data == 0 ];
then
   cp $1 no_duedates
   echo $1 moved to no_duedates
else
	echo $1 Have duedates
fi

if [ $Title == $FileName ];
then 
   echo $1 has no errors
else
   mv -i $1 errors
   echo $1 moved to errors directory
fi

}

for x in `find alice bob olivia -name "*.md"`
do
   Checkfiles $x
done

cat `find alice bob olivia -name "*.md"` > combinedfile.md
echo ALL files have combined
