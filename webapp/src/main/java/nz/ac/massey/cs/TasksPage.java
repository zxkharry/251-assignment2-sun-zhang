package nz.ac.massey.cs;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.resource.ResourceReference;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TasksPage extends WebPage {

	private static final long serialVersionUID = 1L;
	List<Task> isCompleted = new ArrayList<Task>();
	public TasksPage() throws IOException {
		
        add(new EntryForm("entryForm"));
        add(new FeedbackPanel("show"));
        Form listForm = new Form("listForm");
        add(listForm);

		Date now = new Date();
		Label dateTimeLabel = new Label("datetime", now.toString());
		add(dateTimeLabel);

		WicketApplication app = (WicketApplication) this.getApplication();
		TaskList collection = app.getTaskList();
		List<Task> tasks = collection.getTasks();

		
		//call the function which in the TaskList and show the number to the webpage
		Label countLabel = new Label("inactiveCount", new PropertyModel(collection,"InactiveCount"));
		add(countLabel);
		
		//indicator
		Label environmentLabel = new Label("environment", System.getProperty("wicket.configuration"));
		add(environmentLabel);
		
		
		//import the exist files from the data folder
		   List<File> allFiles = new ArrayList<File>();
	       File folderalice = new File("data\\files\\alice");
	       File folderbob = new File("data\\files\\bob");
	       File folderolivia = new File("data\\files\\olivia");
	       File[] filesalice = folderalice.listFiles();
	       File[] filesbob = folderbob.listFiles();
	       File[] filesolivia = folderolivia.listFiles();
	     //put all files to the allFiles list
	       for (File file : filesalice){
	    	   allFiles.add(file);}
	       for (File file : filesbob){
	    	   allFiles.add(file);}
	       for (File file : filesolivia){
	    	   allFiles.add(file);}
	    
	    //read the files from the allFiles list
	    for (File file : allFiles) {
	    String lines = null;   
	    BufferedReader Reader = new BufferedReader(new FileReader(file));;
	    System.out.println(file);
		
		String Title = null;
		//read each line from the files
	    while ((lines = Reader.readLine()) != null){
	    	//init the variablies
	    	String Date = "no duedate";
	   		Boolean complete = false;
	   		String Name = null;
	   		String Description = "No description";
	   		
	    	System.out.println(lines);
	    	
	    	
	    	if(lines.contains("/n")) {
	    		System.out.println("33333333333333333333333");
	    	}
	    	
	    	//Is this line is title or todo task?
	    	if(lines.contains("#")) {
	    		if(!lines.contains("Tasks")) {
	    			Title = lines.substring(2);
	    			System.out.println(Title);
	    		}
	    		continue;
	    	}
	    	
	    	//Is this todo task finished and is this todo task has due date?
	    	if(lines.contains("[X]")) {
		    	if(lines.contains("due")) {
		    		int index1 = lines.indexOf("]");
		    		int index2 = lines.indexOf(":");
		    		Date = lines.substring(index2+1);
		    		Name = lines.substring(index1+2, index2-4);
		    		System.out.println(Date);
		    		System.out.println(Name);
		    	}
		    	else {
		    		int index1 = lines.indexOf("]");
		    		Name = lines.substring(index1+2);
		    		System.out.println(Name);
		    	}
	    		complete = true;
	    	}
	    	else {
		    	if(lines.contains("due")) {
		    		int index1 = lines.indexOf("]");
		    		int index2 = lines.indexOf(":");
		    		Date = lines.substring(index2+1);
		    		Name = lines.substring(index1+2, index2-4);
		    		System.out.println(Date);
		    		System.out.println(Name);
		    		}
		    	else {
		    		int index1 = lines.indexOf("]");
		    		Name = lines.substring(index1+2);
		    		System.out.println(Name);
		    		}
	    	}
	    	
	    //put all the tasks to the whole collection
	    Task task = new Task(Name,Description,Date,Title);
	    task.setComplete(complete);
	    collection.addTask(task);
	    System.out.println(collection.getTasks());
	    
	    }
	    Reader.close();
	    }
	       
		
		PropertyListView taskListView =
				new PropertyListView("task_list", tasks) {
					private static final long serialVersionUID = 1L;

					@Override
					protected void populateItem(ListItem item) {
						
						item.add(new Label("projectTitle"));
						item.add(new Label("name"));
						item.add(new Label("description"));
						item.add(new Label("dueDates"));
						

						item.add(new AjaxCheckBox("completed") {
							@Override
							protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
							}
						});
					}
				};
				
		//select all tasks to make them complete
				
		add(new Link<Void>("selectAll") {
			@Override
			public void onClick() {
				for(Task t: tasks) {
					t.setComplete(true);
				}

			}
		});
		
		//clear all the completed tasks from the whole collection
		add(new Link<Void>("ClearCompleted") {
			@Override
			public void onClick() {
				
				for(Task t: tasks) {
					if(t.isComplete())
						isCompleted.add(t);
				}
				tasks.removeAll(isCompleted);
			}
		});
		
		add(new Link<Void>("WriteToXml") {
			@Override
			public void onClick() {
				XMLEncoder e = null;				
				try {
					System.out.println(tasks);				
					e = new XMLEncoder(
						    new BufferedOutputStream(
						        new FileOutputStream("Tasks.xml")));
					e.writeObject(tasks);
					e.writeObject(isCompleted);
					e.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		
//		the button to show all of the tasks
		Button button = new Button("showAll") {
			public void onSubmit() {
				for (Task task : tasks) {
					info(task.getName() + " " + task.getDescription() + " " + task.getDate() + " " + task.getProjectTitle());
				}
			}
		};
		button.setDefaultFormProcessing(false);
		listForm.add(button);
		
		//the button to show completed tasks
		Button button1 = new Button("showCompleted") {
			public void onSubmit() {
				for (Task task : tasks) {
					if (task.isComplete() == true) {
						info(task.getName() + " " + task.getDescription() + " " + task.getDate() + " " + task.getProjectTitle());
					}
				}
			}
		};
		button1.setDefaultFormProcessing(false);
		listForm.add(button1);
		
		//the button to show active tasks
		Button button2 = new Button("showActive") {
			public void onSubmit() {
				for (Task task : tasks) {
					if (task.isComplete() == false) {
						info(task.getName() + " " + task.getDescription() + " " + task.getDate() + " " + task.getProjectTitle());
					}
				}
			}
		};
		button2.setDefaultFormProcessing(false);
		listForm.add(button2);

		listForm.add(taskListView);
		
		

	}
}
