package nz.ac.massey.cs;

import java.io.Serializable;
import java.util.Date;

// This class models a Task item

public class Task implements Serializable {

    private String description;
    private boolean completed;
    private String name;
    private Integer id;
    private Date date;
    private String dueDates;
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	private String projectTitle;


    public Task(String name,String description,String dueDates, String projectTitle) {
        this.name = name;
        this.description = description;
        this.dueDates = dueDates;
        this.projectTitle = projectTitle;      
        this.completed = false;
    }
    
    public Task()
    {
 	    this.name = "";
        this.description = "";
        this.completed = false;
        this.dueDates = "";
        this.projectTitle = "";
    }
    
    
    public boolean isComplete() {
        return completed;
    }
    public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isCompleted() {
		return completed;
	}


	public void setCompleted(boolean completed) {
		this.completed = completed;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDueDates() {
		return dueDates;
	}


	public void setDueDates(String dueDates) {
		this.dueDates = dueDates;
	}


	public String getProjectTitle() {
		return projectTitle;
	}


	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}


	public void setComplete(boolean complete) { completed = complete; }
}
