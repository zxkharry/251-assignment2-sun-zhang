---
typora-copy-images-to: metrics_analysis_report
typora-root-url: metrics_analysis_report
---

# Readme

## Group Information

| Name         | ID       |
| ------------ | -------- |
| Sun Bowen    | 17138979 |
| Zhang Xikang | 17140345 |

## Instructions

Shell script : 

Please run the shell script in the files folder. 

WebApp : 

The todos in the files will be import to the tasks list when the web page is initialized.

The three filter buttons will show the information you want at the top of the task_list.

​The clear link will clear the completed task from the tasks list and store them to another list which can write to the xml file.

The number of incompleted tasks will refersh itself when you click some button or link.



## Commit IDs

commit 48540973efca6d57acde7858d787eb3f9fd01871 (HEAD -> webapp, origin/webapp)
Author: Sun bowen <123456@qq.com>
Date:   Tue Nov 20 10:02:05 2018 +0800

    empty directories added

commit e90c45b027d9be56232685f16b6511e351737493
Author: Sun bowen <123456@qq.com>
Date:   Tue Nov 20 08:52:56 2018 +0800

    final version

commit 397dcc49ecf32b0b8d21e5e3eb5f70a12307302d
Author: Sun bowen <123456@qq.com>
Date:   Mon Nov 19 19:59:34 2018 +0800

    the second improvement on part 2

commit 518f529de9674d6e0c5f92117107387e95473384
Author: Sun bowen <13231776311@163.com>
Date:   Mon Nov 19 16:47:55 2018 +0800

    be able to show all, active, and completed tasks

commit 87c28d776930464f1678cffc24f0a9bfeba8342c
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:25:22 2018 +0000

    wrongly pushed file

commit 25a13579402371c5799df3fb67419e204519b519 (HEAD -> master)
Author: Sun bowen <123456@qq.com>
Date:   Thu Nov 15 11:08:13 2018 +0800

    try

commit aa4b44bb0e36b0ea6fc390b19e2302ac64f03136 (HEAD -> master, assignment/master)
Author: Harrry-Z <123456@qq.com>
Date:   Wed Nov 14 17:16:58 2018 +0800

    Improved python script

commit d2c5d4454b1aa4c34c5249b6e4df795da6503b96 (HEAD -> webapp)
Author: Sun bowen <123456@qq.com>
Date:   Sun Nov 18 22:18:42 2018 +0800

    item 1 & 6 of part 2

commit 4bc45ffc28feff3986ee16213f8e7775f6570e3f (repo1/scripting, scripting)
Author: Sun bowen <123456@qq.com>
Date:   Sat Nov 17 18:26:04 2018 +0800

    finished shell script

commit 2dedcd9ad8bc8adcdfe1b4db4f33410598ee8481
Author: Harry <13231776311@163.com>
Date:   Sat Nov 17 04:29:13 2018 +0000

    ShellScript.sh (wrongly uploaded) edited online with Bitbucket

commit b54bd56bbf58481d30937832df50993ab686afec
Author: Harry <13231776311@163.com>
Date:   Sat Nov 17 04:22:41 2018 +0000

    file for testing git deleted

commit ec107c6d02f0371659aa47ad38bce7be4c5f93a2
Author: Sun bowen <13231776311@163.com>
Date:   Sat Nov 17 12:20:21 2018 +0800

    try

commit bb67c7e5781b70bd0d4ba675d809488a918fb2ed (HEAD -> master)
Author: Harrry-Z <123456@qq.com>
Date:   Wed Nov 14 20:21:07 2018 +0800

    new script

commit 397dcc49ecf32b0b8d21e5e3eb5f70a12307302d (HEAD -> webapp, origin/webapp)
Author: Sun bowen <123456@qq.com>
Date:   Mon Nov 19 19:59:34 2018 +0800

    the second improvement on part 2

commit 518f529de9674d6e0c5f92117107387e95473384
Author: Sun bowen <13231776311@163.com>
Date:   Mon Nov 19 16:47:55 2018 +0800

    be able to show all, active, and completed tasks

commit 87c28d776930464f1678cffc24f0a9bfeba8342c
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:25:22 2018 +0000

    wrongly pushed file

commit 2b53c7f2129598f68fbf327a6d0c455f38e718d5
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:25:09 2018 +0000

    wrongly pushed file

commit a04da26e36aaa8660f37c8b92a5e40ea978229eb
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:24:58 2018 +0000

    wrongly pushed file

commit a8eb9ced459d6eccfbdcac3137af884e20a2cea5 (HEAD -> master)
Merge: 8be9157 9635b70
Author: Sun bowen <123456@qq.com>
Date:   Wed Nov 14 23:14:16 2018 +0800

    Merge branch 'scripting' of https://bitbucket.org/zxkharry/251-assignment2-sun-zhang

commit 8be9157e1078244d880715e5d80494317bfb75ca
Author: Sun bowen <123456@qq.com>
Date:   Wed Nov 14 22:19:57 2018 +0800

    test for pushing files into scripting branch

commit dea6c12d456e92c8e2cdfae8ab0e89cf9f7d4b74 (origin/master)
Author: Harrry-Z <123456@qq.com>
Date:   Wed Nov 14 14:13:14 2018 +0800

    Improved python script

commit 86290b0c064617a977b8ce2910e2455961e3e78a
Author: Harrry-Z <123456@qq.com>
Date:   Wed Nov 14 01:03:54 2018 +0800

    Demo python scripting for part 1 of the assignment

commit bdd72ebde55a336bdc6d710755c854df72e0189e
Author: Harry <13231776311@163.com>
Date:   Wed Nov 14 09:36:57 2018 +0000

commit 518f529de9674d6e0c5f92117107387e95473384 (HEAD -> webapp, origin/webapp)
Author: Sun bowen <13231776311@163.com>
Date:   Mon Nov 19 16:47:55 2018 +0800

    be able to show all, active, and completed tasks

commit 87c28d776930464f1678cffc24f0a9bfeba8342c
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:25:22 2018 +0000

    wrongly pushed file

commit 2b53c7f2129598f68fbf327a6d0c455f38e718d5
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:25:09 2018 +0000

    wrongly pushed file

commit a04da26e36aaa8660f37c8b92a5e40ea978229eb
Author: Harry <13231776311@163.com>
Date:   Sun Nov 18 14:24:58 2018 +0000

    wrongly pushed file



## Metrics Analysis Report

![CBO](CBO.PNG)

![CC](CC.PNG)

![LOC](LOC.PNG)

![NOM](NOM.PNG)



## PMD Report

src/main/java/nz/ac/massey/cs/EntryForm.java:9:	headerCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:12:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/EntryForm.java:12:	Private field 'nameField' could be made final; it is only initialized in the declaration or constructor.
src/main/java/nz/ac/massey/cs/EntryForm.java:12:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:13:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/EntryForm.java:13:	Private field 'descriptionField' could be made final; it is only initialized in the declaration or constructor.
src/main/java/nz/ac/massey/cs/EntryForm.java:13:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:14:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/EntryForm.java:14:	Private field 'dueDateField' could be made final; it is only initialized in the declaration or constructor.
src/main/java/nz/ac/massey/cs/EntryForm.java:14:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:15:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/EntryForm.java:15:	Private field 'projectTitleField' could be made final; it is only initialized in the declaration or constructor.
src/main/java/nz/ac/massey/cs/EntryForm.java:15:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:18:	Avoid variables with short names like id
src/main/java/nz/ac/massey/cs/EntryForm.java:18:	Parameter 'id' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:18:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:33:	protectedMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/EntryForm.java:37:	Local variable 'name' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:38:	Local variable 'description' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:39:	Local variable 'dueDates' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:40:	Local variable 'projectTitle' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:52:	Local variable 'app' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:53:	Local variable 'collection' could be declared final
src/main/java/nz/ac/massey/cs/EntryForm.java:53:	Potential violation of Law of Demeter (object not created locally)
src/main/java/nz/ac/massey/cs/Task.java:8:	Avoid short class names like Task
src/main/java/nz/ac/massey/cs/Task.java:8:	Classes implementing Serializable should set a serialVersionUID
src/main/java/nz/ac/massey/cs/Task.java:8:	headerCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:10:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:11:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:12:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:13:	Avoid variables with short names like id
src/main/java/nz/ac/massey/cs/Task.java:13:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:14:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:15:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:16:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:20:	Avoid variables with short names like id
src/main/java/nz/ac/massey/cs/Task.java:20:	Parameter 'id' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:20:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:24:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:28:	Parameter 'date' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:28:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:33:	Fields should be declared at the top of the class, before any method declarations, constructors, initializers or inner classes.
src/main/java/nz/ac/massey/cs/Task.java:33:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:36:	Parameter 'description' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:36:	Parameter 'dueDates' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:36:	Parameter 'name' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:36:	Parameter 'projectTitle' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:36:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:44:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:54:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:57:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:62:	Parameter 'description' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:62:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:67:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:72:	Parameter 'completed' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:72:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:77:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:82:	Parameter 'name' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:82:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:87:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:92:	Parameter 'dueDates' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:92:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:97:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:102:	Parameter 'projectTitle' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:102:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/Task.java:107:	Parameter 'complete' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/Task.java:107:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:9:	Classes implementing Serializable should set a serialVersionUID
src/main/java/nz/ac/massey/cs/TaskList.java:9:	headerCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:10:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/TaskList.java:10:	Private field 'collection' could be made final; it is only initialized in the declaration or constructor.
src/main/java/nz/ac/massey/cs/TaskList.java:10:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:12:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:16:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:20:	Parameter 'task' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/TaskList.java:20:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:24:	Parameter 'task' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/TaskList.java:24:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:29:	Found 'UR'-anomaly for variable 't' (lines '29'-'32').
src/main/java/nz/ac/massey/cs/TaskList.java:29:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TaskList.java:31:	Local variable 't' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:17:	Avoid unused imports such as 'java.io.BufferedWriter'
src/main/java/nz/ac/massey/cs/TasksPage.java:22:	Avoid unused imports such as 'java.io.FileWriter'
src/main/java/nz/ac/massey/cs/TasksPage.java:24:	Avoid unused imports such as 'java.lang.reflect.InvocationTargetException'
src/main/java/nz/ac/massey/cs/TasksPage.java:25:	Avoid unused imports such as 'java.lang.reflect.Method'
src/main/java/nz/ac/massey/cs/TasksPage.java:27:	Avoid unused imports such as 'java.util.Arrays'
src/main/java/nz/ac/massey/cs/TasksPage.java:31:	The class 'TasksPage' has a Cyclomatic Complexity of 30 (Highest = 29).
src/main/java/nz/ac/massey/cs/TasksPage.java:31:	The class 'TasksPage' has a Modified Cyclomatic Complexity of 30 (Highest = 29).
src/main/java/nz/ac/massey/cs/TasksPage.java:31:	The class 'TasksPage' has a Standard Cyclomatic Complexity of 30 (Highest = 29).
src/main/java/nz/ac/massey/cs/TasksPage.java:31:	headerCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:34:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/TasksPage.java:34:	To avoid mistakes add a comment at the beginning of the isCompleted field if you want a default access modifier
src/main/java/nz/ac/massey/cs/TasksPage.java:34:	Use explicit scoping instead of the default package private level
src/main/java/nz/ac/massey/cs/TasksPage.java:34:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 'file' (lines '35'-'70').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'178').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	Found 'UR'-anomaly for variable 't' (lines '35'-'190').
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	It is a good practice to call super() in a constructor
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	The constructor 'TasksPage' has a Cyclomatic Complexity of 29.
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	The constructor 'TasksPage' has a Modified Cyclomatic Complexity of 29.
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	The constructor 'TasksPage' has a Standard Cyclomatic Complexity of 29.
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	The constructor with 0 parameters has an NCSS line count of 110
src/main/java/nz/ac/massey/cs/TasksPage.java:35:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:39:	Local variable 'listForm' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:42:	Local variable 'now' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:43:	Local variable 'dateTimeLabel' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:46:	Local variable 'app' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:47:	Local variable 'collection' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:48:	Local variable 'tasks' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:52:	Local variable 'countLabel' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:56:	Local variable 'environmentLabel' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:61:	Local variable 'allFiles' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:62:	Local variable 'folderalice' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:63:	Local variable 'folderbob' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:64:	Local variable 'folderolivia' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:65:	Local variable 'filesalice' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:66:	Local variable 'filesbob' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:67:	Local variable 'filesolivia' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:69:	Local variable 'file' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:71:	Local variable 'file' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:73:	Local variable 'file' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:77:	Local variable 'file' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:78:	Found 'DD'-anomaly for variable 'lines' (lines '78'-'84').
src/main/java/nz/ac/massey/cs/TasksPage.java:79:	An empty statement (semicolon) not part of a loop
src/main/java/nz/ac/massey/cs/TasksPage.java:79:	Avoid instantiating new objects inside loops
src/main/java/nz/ac/massey/cs/TasksPage.java:79:	Local variable 'Reader' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:79:	Variables should start with a lowercase character, 'Reader' starts with uppercase character.
src/main/java/nz/ac/massey/cs/TasksPage.java:80:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Found 'DD'-anomaly for variable 'Title' (lines '82'-'101').
src/main/java/nz/ac/massey/cs/TasksPage.java:82:	Variables should start with a lowercase character, 'Title' starts with uppercase character.
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Avoid assignments in operands
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DD'-anomaly for variable 'lines' (lines '84'-'78').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:84:	Found 'DU'-anomaly for variable 'lines' (lines '84'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'112').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Found 'DD'-anomaly for variable 'Date' (lines '86'-'128').
src/main/java/nz/ac/massey/cs/TasksPage.java:86:	Variables should start with a lowercase character, 'Date' starts with uppercase character.
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:87:	Found 'DD'-anomaly for variable 'complete' (lines '87'-'122').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'113').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'119').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Found 'DD'-anomaly for variable 'Name' (lines '88'-'129').
src/main/java/nz/ac/massey/cs/TasksPage.java:88:	Variables should start with a lowercase character, 'Name' starts with uppercase character.
src/main/java/nz/ac/massey/cs/TasksPage.java:89:	Local variable 'Description' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:89:	Variables should start with a lowercase character, 'Description' starts with uppercase character.
src/main/java/nz/ac/massey/cs/TasksPage.java:91:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:95:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:102:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:110:	Local variable 'index1' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:110:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:111:	Local variable 'index2' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:111:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:114:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:115:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:118:	Local variable 'index1' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:118:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:120:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:126:	Local variable 'index1' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:126:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:127:	Local variable 'index2' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:127:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:130:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:131:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:134:	Local variable 'index1' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:134:	String.indexOf(char) is faster than String.indexOf(String).
src/main/java/nz/ac/massey/cs/TasksPage.java:136:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:141:	Avoid instantiating new objects inside loops
src/main/java/nz/ac/massey/cs/TasksPage.java:141:	Local variable 'task' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:144:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:151:	Local variable 'taskListView' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:153:	Found 'DU'-anomaly for variable 'serialVersionUID' (lines '153'-'257').
src/main/java/nz/ac/massey/cs/TasksPage.java:156:	Parameter 'item' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:156:	protectedMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:166:	Document empty method body
src/main/java/nz/ac/massey/cs/TasksPage.java:166:	Parameter 'ajaxRequestTarget' is not assigned and could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:166:	protectedMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:176:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:177:	Local variable 't' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:187:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:189:	Local variable 't' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:190:	Avoid using if statements without curly braces
src/main/java/nz/ac/massey/cs/TasksPage.java:199:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Avoid variables with short names like e
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:200:	Found 'DD'-anomaly for variable 'e' (lines '200'-'203').
src/main/java/nz/ac/massey/cs/TasksPage.java:202:	System.out.println is used
src/main/java/nz/ac/massey/cs/TasksPage.java:211:	Avoid printStackTrace(); use a logger call instead.
src/main/java/nz/ac/massey/cs/TasksPage.java:217:	Local variable 'button' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:218:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:228:	Local variable 'button1' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:229:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:231:	Avoid unnecessary comparisons in boolean expressions
src/main/java/nz/ac/massey/cs/TasksPage.java:241:	Local variable 'button2' could be declared final
src/main/java/nz/ac/massey/cs/TasksPage.java:242:	publicMethodCommentRequirement Required
src/main/java/nz/ac/massey/cs/TasksPage.java:244:	Avoid unnecessary comparisons in boolean expressions
src/main/java/nz/ac/massey/cs/WicketApplication.java:11:	Each class should declare at least one constructor
src/main/java/nz/ac/massey/cs/WicketApplication.java:13:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/WicketApplication.java:13:	fieldCommentRequirement Required
src/main/java/nz/ac/massey/cs/WicketApplication.java:35:	publicMethodCommentRequirement Required
src/test/java/nz/ac/massey/cs/Start.java:19:	Comment is too large: Line too long
src/test/java/nz/ac/massey/cs/Start.java:23:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/test/java/nz/ac/massey/cs/Start.java:29:	Parameter 'args' is not assigned and could be declared final
src/test/java/nz/ac/massey/cs/Start.java:33:	Local variable 'server' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:35:	Local variable 'http_config' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:35:	Only variables that are final should contain underscores (except for underscores in standard prefix/suffix), 'http_config' is not final.
src/test/java/nz/ac/massey/cs/Start.java:40:	Local variable 'http' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:46:	Local variable 'keystore' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:47:	Potential violation of Law of Demeter (object not created locally)
src/test/java/nz/ac/massey/cs/Start.java:56:	Local variable 'sslContextFactory' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:61:	Local variable 'https_config' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:61:	Only variables that are final should contain underscores (except for underscores in standard prefix/suffix), 'https_config' is not final.
src/test/java/nz/ac/massey/cs/Start.java:64:	Local variable 'https' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:70:	System.out.println is used
src/test/java/nz/ac/massey/cs/Start.java:71:	System.out.println is used
src/test/java/nz/ac/massey/cs/Start.java:73:	System.out.println is used
src/test/java/nz/ac/massey/cs/Start.java:76:	Avoid variables with short names like bb
src/test/java/nz/ac/massey/cs/Start.java:76:	Local variable 'bb' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:81:	Comment is too large: Line too long
src/test/java/nz/ac/massey/cs/Start.java:83:	Comment is too large: Line too long
src/test/java/nz/ac/massey/cs/Start.java:92:	Local variable 'mBeanServer' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:93:	Local variable 'mBeanContainer' could be declared final
src/test/java/nz/ac/massey/cs/Start.java:102:	Avoid catching generic exceptions such as NullPointerException, RuntimeException, Exception in try-catch block
src/test/java/nz/ac/massey/cs/Start.java:104:	Avoid printStackTrace(); use a logger call instead.
src/test/java/nz/ac/massey/cs/Start.java:105:	System.exit() should not be used in J2EE/JEE apps
src/test/java/nz/ac/massey/cs/TestHomePage.java:10:	Each class should declare at least one constructor
src/test/java/nz/ac/massey/cs/TestHomePage.java:12:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/test/java/nz/ac/massey/cs/TestHomePage.java:12:	fieldCommentRequirement Required
src/test/java/nz/ac/massey/cs/TestHomePage.java:15:	publicMethodCommentRequirement Required
src/test/java/nz/ac/massey/cs/TestHomePage.java:21:	publicMethodCommentRequirement Required